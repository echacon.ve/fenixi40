package org.echacon.fenix.cargadorFenix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.echacon.fenix.cargadorFenix.process.CargaDatosBasicos;
import org.echacon.fenix.cargadorFenix.process.contenedores.OrganizacionCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ServicioManufacturaCont;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml.LectorOrganizacionXML;
import org.echacon.fenix.cargadorFenix.process.lectores.redesPetri.LectorPNML;
import org.echacon.fenix.cargadorFenix.service.EmpresaService;
import org.echacon.fenix.entity.base.Organizacion;
import org.echacon.fenix.entity.recurso.UnidadFuncional;
import org.echacon.fenix.entity.serviciosManufactura.ProveedorServicio;
import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;
import org.echacon.fenix.repository.actividad.ActividadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@EntityScan(basePackages = {"org.echacon.fenix.entity"})
@EnableJpaRepositories(basePackages = "org.echacon.fenix.repository")

public class CargadorFenix {

	static ApplicationContext ctx;

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(CargadorFenix.class, args);
		String[] beans = ctx.getBeanDefinitionNames();
        Arrays.sort(beans);
        System.out.println("Cargador Fenix");
        for (String bean : beans) {
        	if(bean.contains("Repository") || bean.contains("Service")) {
          		System.out.println(bean);
        	}
        }
 
        /*
         * Creacion de la clase de carga de datos basicos
         */

        CargaDatosBasicos cdb = new CargaDatosBasicos();
        cdb.setApplicationContext(ctx);
       
       /*
        * En el directorio de lectura esta un archivo texto denominado tareas.txt
        * En ese archivo están las tareas ppor ejecutar para la carga de datos de la
        * empresa, carga de los modelos de producto, carga de los modelos de proceso
        */
    	File dirLec = new File("directorioLectura");
    	File tareasARealizar;
    	if(dirLec.isDirectory()) {
    		tareasARealizar = new File("directorioLectura/tareas.txt");
    		if(tareasARealizar.isFile()) {
    			try {
    				BufferedReader bf = new BufferedReader(new FileReader(tareasARealizar));
					String lineaLeida;
					while((lineaLeida = bf.readLine()) != null) {
						lineaLeida=lineaLeida.trim();
						lineaLeida = lineaLeida.replaceAll("\\s{2,}", " ");
						String[] campos = lineaLeida.split(" ");
						System.out.println(lineaLeida);
						if(lineaLeida.contains("cargar datosempresa")) {
							cdb.lecturaDatosEmpresa(dirLec+"/"+campos[2]);
						}
						if(lineaLeida.contains("cargar organizacion")) {
							cdb.lecturaDatosOrganizacion(dirLec+"/"+campos[2]);
						}
						if(lineaLeida.contains("cargar serviciosManufactura")) {
							cdb.lecturaDatosServiciosManufactura(dirLec+"/"+campos[2]);
						}
						if(lineaLeida.contains("cargar unidadesFuncionales")) {
							cdb.lecturaDatosUnidadFuncional(dirLec+"/"+campos[2]);
						}
						if(lineaLeida.contains("cargar excelMateriales")) {
							cdb.lecturaDatosExcelMateriales(dirLec+"/"+campos[2]);
						}
						if(lineaLeida.contains("cargar productos")) {
							cdb.lecturaDatosProductos(dirLec+"/"+campos[2]);
						}
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
    	
    	
        
//        EmpresaService empser = (EmpresaService) ctx.getBean("empresaService");
//        
//        List<ServicioManufactura> listaSinUnidad = empser.buscarServicioSinProveedor();
//        System.out.println(listaSinUnidad.size());
//        List<ServicioManufactura> listaServicios = empser.listarServicios();
//        for(ServicioManufactura sm : listaServicios) {
//        	System.out.println("servicio manufactura" + sm.getNombreServicio());
//        	List<UnidadFuncional> luf = empser.buscarProveedor(sm);
//        	for(UnidadFuncional uf : luf) {
//        		System.out.println("unidad Funcional " + uf.getNombre()) ;
//        	}
//        }
//        empser.asociarUnidadFuncionalServicios();
        
	}
}
