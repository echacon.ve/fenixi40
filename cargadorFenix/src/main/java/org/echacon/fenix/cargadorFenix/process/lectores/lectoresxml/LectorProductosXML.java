package org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.echacon.fenix.cargadorFenix.process.contenedores.InsumoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ModeloProductoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ProductoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.PropiedadCont;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorProductosXML {
	List<ProductoCont> listaproductos;
	public List<ProductoCont> getListaproductos() {
		return listaproductos;
	}
	public void setListaproductos(List<ProductoCont> listaproductos) {
		this.listaproductos = listaproductos;
	}


	public int leer(String nomArch) {
		int res = 0;
		listaproductos = new ArrayList<>();
		
		File fXmlFile = new File(nomArch);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList productos = doc.getElementsByTagName("producto");

			int nroProd = productos.getLength();
			for(int i=0; i<nroProd;i++){
				Node producto = productos.item(i);
				ProductoCont nprod = new ProductoCont();
				nprod.setPropiedades(new ArrayList<>());
				nprod.setModelosProducto(new ArrayList<>());
				String nomProducto = ((Element) producto).getAttribute("nombre");
				nprod.setNombre(nomProducto);
				String nomDescripcion = nomProducto = ((Element) producto).getAttribute("descripcion");
				nprod.setDescripcion(nomDescripcion);
				NodeList nodosProducto = producto.getChildNodes();
				for(int j=0; j<nodosProducto.getLength();j++) {
					Node nodo = nodosProducto.item(j);
					if(nodo.getNodeName() == "propiedades") {
						NodeList propiedades = nodo.getChildNodes();
						for(int k=0; k<propiedades.getLength();k++) {
							
							Node npropiedad = propiedades.item(k);  
							if(npropiedad.getNodeName() == "propiedad") {
								String nombre =  ((Element) npropiedad).getAttribute("nombre");
								PropiedadCont propCont = new PropiedadCont();
								propCont.setNombre(nombre);
								NodeList valoresPropiedad = npropiedad.getChildNodes();
								Node valor = valoresPropiedad.item(0);
								if(valor.getNodeName().contentEquals("numerico")) {
									propCont.setCadena(false);
									String vmax	= ((Element) valor).getAttribute("valmax");
									String vmin	= ((Element) valor).getAttribute("valmin");
									propCont.setValormax(Float.parseFloat(vmax));
									propCont.setValormin(Float.parseFloat(vmin));
								}
								if(valor.getNodeName().contentEquals("caracter")) {
									String cadenaCar = ((Element) valor).getAttribute("valor");
									propCont.setEspecificacion(cadenaCar);
								}
								nprod.getPropiedades().add(propCont);
							}
						}
					}
					if(nodo.getNodeName() == "modelos") {
						NodeList modelos = nodo.getChildNodes();
						for(int k=0; k<modelos.getLength();k++) {
							Node nmodelo = modelos.item(k);
							String nmodeloname = nmodelo.getNodeName();
							if(nmodeloname.contentEquals("modelo")) {
								ModeloProductoCont mpc = new ModeloProductoCont();
								String codigo = ((Element) nmodelo).getAttribute("codigo");
								String nombreModelo = ((Element) nmodelo).getAttribute("nombre");
								mpc.setCodigo(codigo);
								mpc.setNombreModelo(nombreModelo);
								mpc.setInsumos(new ArrayList<>());
								NodeList modeloElem = nmodelo.getChildNodes();								
								for(int l=0;l<modeloElem.getLength(); l++) {
									Node elemento = modeloElem.item(l);
									String nomElem = elemento.getNodeName();
									switch (nomElem) {
									case "formula":
										String cantidadProducto = ((Element) elemento).getAttribute("cantidad");
										mpc.setCantidadReferencia(Float.parseFloat(cantidadProducto));
										NodeList insumos = elemento.getChildNodes();
										for(int l1=0; l1<insumos.getLength();l1++) {
											Node insumo = insumos.item(l1);
											if(insumo.getNodeName().contentEquals("insumo")) {
												InsumoCont ic = new InsumoCont();
												String nomInsumo = ((Element) insumo).getAttribute("codigo");
												ic.setNombre(nomInsumo);
												String stcant = ((Element) insumo).getAttribute("cantidad");
												ic.setCantidad(Float.parseFloat(stcant));
												mpc.getInsumos().add(ic);
											}
										}
										break;
									case "procedimiento":
										String nomProcedimiento = ((Element) elemento).getAttribute("id");
										mpc.setNombreDinamica(nomProcedimiento);
										break;

									default:
										break;
									}
									
								}
								nprod.getModelosProducto().add(mpc);
							} // Fin del analisis de un modelo
						}  
					}  // Fin lazo de analisis de la seccion modelos
				}
				listaproductos.add(nprod);
			}
			res =1;
			
		} catch (ParserConfigurationException e) {
			res=-1;
			e.printStackTrace();
		} catch (SAXException e) {
			res=-2;
			e.printStackTrace();
		} catch (IOException e) {
			res=-3;
			e.printStackTrace();
		}
		
		return res;
	}
}
