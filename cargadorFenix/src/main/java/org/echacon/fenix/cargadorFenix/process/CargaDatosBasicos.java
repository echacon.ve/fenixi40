package org.echacon.fenix.cargadorFenix.process;

import java.util.ArrayList;
import java.util.List;

import org.echacon.fenix.cargadorFenix.process.contenedores.EmpresaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.MateriaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.OrganizacionCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ProductoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ServicioManufacturaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.UnidadFuncionalCont;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresexcel.LectorMaterialExcel;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml.LectorEmpresaXML;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml.LectorOrganizacionXML;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml.LectorProductosXML;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml.LectorServiciosManufacturaXML;
import org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml.LectorUnidadFuncionalXML;
import org.echacon.fenix.cargadorFenix.service.EmpresaService;
import org.echacon.fenix.cargadorFenix.service.MateriaService;
import org.echacon.fenix.cargadorFenix.service.ProductoService;
import org.echacon.fenix.entity.base.Empresa;
import org.echacon.fenix.entity.materia.Materia;
import org.echacon.fenix.entity.producto.Producto;
import org.echacon.fenix.entity.recurso.UnidadFuncional;
import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class CargaDatosBasicos implements ApplicationContextAware{
	ApplicationContext ctx;
	EmpresaService empser;
	MateriaService matser;
	ProductoService prodser;

	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.ctx = ctx;
		empser = (EmpresaService) ctx.getBean("empresaService");
		matser = (MateriaService) ctx.getBean("materiaService");
		prodser = (ProductoService) ctx.getBean("productoService");
	}
	
	public int lecturaDatosEmpresa(String nombreArchivo) {
		LectorEmpresaXML lexml = new LectorEmpresaXML();
		int res = lexml.leer(nombreArchivo);
		if(res == 1) {
			EmpresaCont ec = lexml.getEc();
			Empresa emp = empser.crearEmpresa(ec);
			if(emp == null) {
				return -1;
			}
		}
		return 0;
	}
	public int lecturaDatosOrganizacion(String nombreArchivo) {
		 LectorOrganizacionXML lxml = new LectorOrganizacionXML();
		 int res = 0;
		 res = lxml.leer(nombreArchivo);
		 if(res == 1) {
			 List<OrganizacionCont> organizaciones = lxml.getOrganizaciones();
			 empser.crearVariasOrg(organizaciones);
		 }
		 return res;
	}
	public int lecturaDatosUnidadFuncional(String nombreArchivo) {
		LectorUnidadFuncionalXML lxml = new LectorUnidadFuncionalXML();
		int res = 0;
		res= lxml.leer(nombreArchivo);
		 if(res == 1) {
			 List<UnidadFuncionalCont> unidades = lxml.getUnidades();
			 List<UnidadFuncional> listaUF = empser.crearListaUnidadesFuncionales(unidades);
		 }
		
		return res;
	}
	public int lecturaDatosServiciosManufactura(String nombreArchivo) {
		int res =0;
		LectorServiciosManufacturaXML lxml = new LectorServiciosManufacturaXML();
		res = lxml.leer(nombreArchivo);
		if(res == 1) {
			List<ServicioManufacturaCont> listasmc = lxml.getListaServicio();
			List<ServicioManufactura> listaSM = empser.crearListaServicios(listasmc);
		}
    	return res;
	}

	public void lecturaDatosExcelMateriales(String nomArchivo) {
		
		LectorMaterialExcel lm = new LectorMaterialExcel();
		int res = lm.leerArchivo(nomArchivo);
		if(res == 1) {
			List<MateriaCont> listaMateriales = lm.getListaMateriales();
			List<Materia> nuevasMaterias = new ArrayList<>();
			for(MateriaCont mc : listaMateriales) {
				Materia nm = matser.crearMateria(mc);
				if(nm != null) {
					matser.salvarMateria(nm);
				}
			}
		}
	}

	public void lecturaDatosProductos(String nomArchivo) {
		LectorProductosXML lector = new LectorProductosXML();
		int res = lector.leer(nomArchivo);
		if(res == 1) {
			List<ProductoCont> lista = lector.getListaproductos();
			for(ProductoCont pc : lista) {
				Producto prod = prodser.crearProducto(pc);
				if(prod != null) {
					System.out.println("Producto creado " + pc.getNombre());
					int codigoError = prodser.obtenerCodigoError();
					if(codigoError == 0) {
						System.out.println("Salvar el producto");
						prodser.salvarProducto(prod);
					}
					else {
						System.out.println("Hay un error en el producto " + codigoError);
					}
				}
				
				
			}
			
		}
		
	}
}
