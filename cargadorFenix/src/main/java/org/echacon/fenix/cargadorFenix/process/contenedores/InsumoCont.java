package org.echacon.fenix.cargadorFenix.process.contenedores;

public class InsumoCont {
	private String nombre;
	private float cantidad;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getCantidad() {
		return cantidad;
	}
	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}
}
