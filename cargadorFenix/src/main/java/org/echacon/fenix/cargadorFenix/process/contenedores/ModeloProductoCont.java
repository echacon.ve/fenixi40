package org.echacon.fenix.cargadorFenix.process.contenedores;

import java.util.List;

public class ModeloProductoCont {
	private String nombreProducto;
	private float cantidadReferencia;
	private String nombreModelo;
	private String codigo;
	private List<InsumoCont> insumos;
	private String nombreDinamica;
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public float getCantidadReferencia() {
		return cantidadReferencia;
	}
	public void setCantidadReferencia(float cantidadReferencia) {
		this.cantidadReferencia = cantidadReferencia;
	}
	public String getNombreModelo() {
		return nombreModelo;
	}
	public void setNombreModelo(String nombreModelo) {
		this.nombreModelo = nombreModelo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public List<InsumoCont> getInsumos() {
		return insumos;
	}
	public void setInsumos(List<InsumoCont> insumos) {
		this.insumos = insumos;
	}
	public String getNombreDinamica() {
		return nombreDinamica;
	}
	public void setNombreDinamica(String nombreDinamica) {
		this.nombreDinamica = nombreDinamica;
	}
	
}
