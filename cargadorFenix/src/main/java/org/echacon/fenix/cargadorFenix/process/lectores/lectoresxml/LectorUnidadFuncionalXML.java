package org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.echacon.fenix.cargadorFenix.process.contenedores.EmpresaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.OrganizacionCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.UnidadFuncionalCont;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorUnidadFuncionalXML {
	private List<UnidadFuncionalCont> unidades;
	public List<UnidadFuncionalCont> getUnidades() {
		return unidades;
	}
	public void setUnidades(List<UnidadFuncionalCont> unidades) {
		this.unidades = unidades;
	}


	public int leer(String nomArch) {
		int res = 0;
		UnidadFuncionalCont ufc;
		File fXmlFile = new File(nomArch);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList nUF = doc.getElementsByTagName("unidades");
			if(nUF.getLength() == 1) {
				NodeList nodosUF = nUF.item(0).getChildNodes();
				unidades= new ArrayList<>();
				for(int i = 0; i < nodosUF.getLength(); i++) {
					Node nodo = nodosUF.item(i);
					if(nodo.getNodeName() == "unidadFuncional") {
						ufc = new UnidadFuncionalCont();
						ufc.setListaServicios(new ArrayList<>());
						NodeList nElementos = nodo.getChildNodes();
						for(int j=0;j<nElementos.getLength();j++) {
							Node elemento = nElementos.item(j);
							if(elemento.getNodeName() == "nombre") {
								ufc.setNombre(elemento.getTextContent().trim());
							}
							if(elemento.getNodeName() == "serviciosManufactura") {
								NodeList servicios = elemento.getChildNodes();
								for(int k=0; k<servicios.getLength(); k++) {
									Node servicio = servicios.item(k);
									if(servicio.getNodeName()=="servicio") {
										String nomServicio = servicio.getTextContent().trim();
										ufc.getListaServicios().add(nomServicio);
									}
								}
							}
							if(elemento.getNodeName()=="organizacion") {
								ufc.setOrganizacion(elemento.getTextContent().trim());	
							}
							if(elemento.getNodeName()=="descripcion") {
								ufc.setDescripcion(elemento.getTextContent().trim());
							}
						}
						unidades.add(ufc);
					}
				}
				res =1;	
			} else
				res =-1;
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}

}
