package org.echacon.fenix.cargadorFenix.service;

import java.util.ArrayList;
import java.util.List;

import org.echacon.fenix.cargadorFenix.process.contenedores.MateriaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.PropiedadCont;
import org.echacon.fenix.entity.base.Empresa;
import org.echacon.fenix.entity.materia.Materia;
import org.echacon.fenix.entity.materia.PropiedadMateria;
import org.echacon.fenix.repository.materia.MateriaRepository;
import org.echacon.fenix.repository.materia.PropiedadMateriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("materiaService")
public class MateriaServiceImpl implements MateriaService{
	@Autowired
	MateriaRepository mrep;
	@Autowired
	PropiedadMateriaRepository pmrp;
	
	@Override
	public Materia crearMateria(MateriaCont mc) {
		List<Materia> res1 = mrep.findByNombre(mc.getNombre());
		if(res1.size()!=0) 
			return null;
		Materia mat = new Materia();
		mat.setNombre(mc.getNombre());
		mat.setCodigo(mc.getCodigo());
		mat.setDescripcion(mc.getDescripcion());
		mat.setAdquirido(mc.isAdquirido());
		mat.setProducido(mc.isProducido());
		mat.setGranel(mc.isGranel());
		mat.setUnidad(mc.isUnidad());
		mat.setLiquido(mc.isLiquido());
		mat.setGas(mc.isGas());
		mat.setSolido(mc.isSolido());
		mat.setPropiedades(new ArrayList());
		for(PropiedadCont pc : mc.getListaPropiedades()) {
			PropiedadMateria pm = new PropiedadMateria();
			pm.setNombre(pc.getNombre());
			pm.setMateria(mat);
			pm.setCadena(pc.isCadena());
			pm.setRangoNumerico(!pc.isCadena());
			pm.setValorCadena(pc.getEspecificacion());
			pm.setValMin(pc.getValormin());
			pm.setValMax(pc.getValormax());
			mat.getPropiedades().add(pm);
		}
		return mat;
	}

	@Override
	public Materia encontrarPorNombre(String nombre) {
		List<Materia> res1 = mrep.findByNombre(nombre);
		if(res1.size()!=1) 
			return null;
		Materia mat = res1.get(0);
		List<PropiedadMateria> lpm = pmrp.findByMateria(mat);
		mat.setPropiedades(lpm);
		return mat;
	}

	@Override
	public Materia encontrarPorCodigo(String codigo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvarMateria(Materia mat) {
		mrep.save(mat);
		for(PropiedadMateria pm : mat.getPropiedades()) {
			pmrp.save(pm);
		}	
	}

}
