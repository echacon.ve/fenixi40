package org.echacon.fenix.cargadorFenix.process.contenedores;

import java.util.List;

public class OrganizacionCont {
	private String nombre;
	private String descripcion;
	private String organizacionPadre;
	private List<String> listaServicios;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOrganizacionPadre() {
		return organizacionPadre;
	}
	public void setOrganizacionPadre(String organizacionPadre) {
		this.organizacionPadre = organizacionPadre;
	}
	public List<String> getListaServicios() {
		return listaServicios;
	}
	public void setListaServicios(List<String> listaServicios) {
		this.listaServicios = listaServicios;
	}
}
