package org.echacon.fenix.cargadorFenix.process.contenedores;

import java.util.List;

public class ProductoCont {
	private String nombre;
	private String descripcion;
	private List<PropiedadCont> propiedades;
	private List<ModeloProductoCont> modelosProducto;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<PropiedadCont> getPropiedades() {
		return propiedades;
	}
	public void setPropiedades(List<PropiedadCont> propiedades) {
		this.propiedades = propiedades;
	}
	public List<ModeloProductoCont> getModelosProducto() {
		return modelosProducto;
	}
	public void setModelosProducto(List<ModeloProductoCont> modelosProducto) {
		this.modelosProducto = modelosProducto;
	}
	
}
