package org.echacon.fenix.cargadorFenix.process.lectores.redesPetri;

import java.util.ArrayList;
import java.util.List;



public class RedPetri {
	private String nombre;
	private List<Transicion> listaTransicion;
	private List<Lugar> listaLugar;
	private List<Arco> listaArco;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Transicion> getListaTransicion() {
		return listaTransicion;
	}

	public void setListaTransicion(List<Transicion> listaTransicion) {
		this.listaTransicion = listaTransicion;
	}

	public List<Lugar> getListaLugar() {
		return listaLugar;
	}

	public void setListaLugar(List<Lugar> listaLugar) {
		this.listaLugar = listaLugar;
	}

	public List<Arco> getListaArco() {
		return listaArco;
	}

	public void setListaArco(List<Arco> listaArco) {
		this.listaArco = listaArco;
	}

	public void inicializar() {
		listaArco = new ArrayList<Arco>();
		listaLugar = new ArrayList<Lugar>();
		listaTransicion = new ArrayList<Transicion>();
	}
	
	public void agregarTransicion(Transicion transicion){
		listaTransicion.add(transicion);
	}
	
	public void agregarLugar(Lugar lugar){
		listaLugar.add(lugar);
	}
	
	public void agregarArco(Arco arco){
		listaArco.add(arco);
	}

}
