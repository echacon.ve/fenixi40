package org.echacon.fenix.cargadorFenix.process.contenedores;

import java.util.List;

public class UnidadFuncionalCont {
	private String nombre;
	private List<String> listaServicios;
	private String organizacion;
	private String descripcion;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getListaServicios() {
		return listaServicios;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setListaServicios(List<String> listaServicios) {
		this.listaServicios = listaServicios;
	}
	public String getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}
}
