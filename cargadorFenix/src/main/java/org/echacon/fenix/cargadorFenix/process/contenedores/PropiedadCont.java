package org.echacon.fenix.cargadorFenix.process.contenedores;

public class PropiedadCont {
	private String nombre;
	private float valormin;
	private float valormax;
	private boolean cadena;
	private String especificacion;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getValormin() {
		return valormin;
	}
	public void setValormin(float valormin) {
		this.valormin = valormin;
	}
	public float getValormax() {
		return valormax;
	}
	public void setValormax(float valormax) {
		this.valormax = valormax;
	}
	public boolean isCadena() {
		return cadena;
	}
	public void setCadena(boolean cadena) {
		this.cadena = cadena;
	}
	public String getEspecificacion() {
		return especificacion;
	}
	public void setEspecificacion(String especificacion) {
		this.especificacion = especificacion;
	}
	
	
	
}
