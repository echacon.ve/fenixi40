package org.echacon.fenix.cargadorFenix.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.echacon.fenix.cargadorFenix.process.contenedores.EmpresaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.OrganizacionCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ServicioManufacturaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.UnidadFuncionalCont;
import org.echacon.fenix.entity.base.Empresa;
import org.echacon.fenix.entity.base.Organizacion;
import org.echacon.fenix.entity.recurso.UnidadFuncional;
import org.echacon.fenix.entity.serviciosManufactura.ProveedorServicio;
import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;
import org.echacon.fenix.repository.base.EmpresaRepository;
import org.echacon.fenix.repository.base.OrganizacionRepository;
import org.echacon.fenix.repository.recurso.UnidadFuncionalRepository;
import org.echacon.fenix.repository.serviciosManufactura.ProveedorServicioRepository;
import org.echacon.fenix.repository.serviciosManufactura.ServicioManufacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("empresaService")
public class EmpresaServiceImpl implements EmpresaService {
	@Autowired
	EmpresaRepository erp;
	@Autowired
	OrganizacionRepository orp;
	@Autowired
	ServicioManufacturaRepository smrp;
	@Autowired
	UnidadFuncionalRepository ufrp;
	@Autowired
	ProveedorServicioRepository psrp;
	
	@Override
	public Empresa crearEmpresa(EmpresaCont ec) {
		List<Empresa> res1 = erp.findByNombre(ec.getNombre());
		Empresa res = new Empresa();
		if(res1.size() == 0) {
			res = new Empresa();
			res.setNombre(ec.getNombre());
			res.setFechaSistema(LocalDate.now());
			res.setContacto(ec.getContacto());
			res.setDescripcion(ec.getDescripcion());
			erp.save(res);
		}
		return res;
	}
	@Override
	public void salvar(Empresa emp) {
		erp.save(emp);
	}

	@Override
	public Organizacion crearOrganizacion(OrganizacionCont oc) {
		List<Organizacion> res = orp.findByNombre(oc.getNombre());
		Organizacion org = null;
		if(res.size() == 0) {
			org = new Organizacion();
			org.setNombre(oc.getNombre());
			org.setDescripcion(oc.getDescripcion());
		}
		return org;
	}
	@Override
	public Organizacion buscarOrganizacion(String nombre) {
		List<Organizacion> listaOrg = orp.findByNombre(nombre);
		if(listaOrg.size() == 1) 
			return listaOrg.get(0);
		return null;
	}
	@Override
	public List<Organizacion> crearVariasOrg(List<OrganizacionCont> organizacion) {
		List<Organizacion> res = new ArrayList<>();
		for(OrganizacionCont oc: organizacion) {
			Organizacion org = crearOrganizacion(oc);
			if(org != null) {
				Organizacion org2 = buscarOrganizacion(oc.getOrganizacionPadre());
				if(org2 != null) {
					org.setDependeDe(org2);
				}
				orp.save(org);
				res.add(org);
			}
		}
		return res;
	}

	@Override
	public void salvarOrganizacion(Organizacion org) {
		orp.save(org);
	}
	@Override
	public List<UnidadFuncional> crearListaUnidadesFuncionales(List<UnidadFuncionalCont> lista) {
		List<UnidadFuncional> luf = new ArrayList<>();
		for(UnidadFuncionalCont ufc:lista) {
			List<UnidadFuncional> lufnombre = ufrp.findByNombre(ufc.getNombre());
			if(lufnombre.size() == 0) {
				UnidadFuncional uf = new UnidadFuncional();
				uf.setNombre(ufc.getNombre());
				uf.setDescripcion(ufc.getDescripcion());
				List<Organizacion> lorg = orp.findByNombre(ufc.getOrganizacion());
				if(lorg.size()==1) {
					uf.setOrganizacion(lorg.get(0));
				}
				ufrp.save(uf);
				// Lectura de los servicios de manufactura existentes y asignación de los
				// esecificados
				List<ServicioManufactura> lsm = smrp.findAll();
				for(String nomser : ufc.getListaServicios()) {
					ServicioManufactura smas = null;
					for(ServicioManufactura sm : lsm) {
						if(sm.getNombreServicio().contentEquals(nomser)) {
							smas = sm;
							break;
						}
					}
					ProveedorServicio ps = new ProveedorServicio();
					ps.setNombreServicio(nomser);
					ps.setProveedorServicio(uf.getNombre());
					ps.setUnidadFuncional(uf);
					psrp.save(ps);
				}
			}
		}
		return luf;
	}

	@Override
	public boolean asociarOrgAEmp(Empresa emp, Organizacion org) {
		
		return false;
	}

	@Override
	public boolean asociarOrgAOrg(Organizacion org1, Organizacion org2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ServicioManufactura crearServ(ServicioManufacturaCont smc) {
		List<ServicioManufactura> res = smrp.findByNombreServicio(smc.getNombre());
		ServicioManufactura serman = null;
		if(res.size() == 0) {
			serman = new ServicioManufactura();
			serman.setNombreServicio(smc.getNombre());
			serman.setDescripcion(smc.getDescripcion());
		}
		return serman;
	}
	
	@Override
	public List<ServicioManufactura> crearListaServicios(List<ServicioManufacturaCont> listasmc) {
		List<ServicioManufactura> busqueda;
		List<ServicioManufactura> res = new ArrayList<>();
		for(ServicioManufacturaCont smc : listasmc) {
			busqueda = smrp.findByNombreServicio(smc.getNombre());
			if(busqueda.size()==0) {
				ServicioManufactura nsm = new ServicioManufactura();
				nsm.setNombreServicio(smc.getNombre());
				nsm.setDescripcion(smc.getDescripcion());
				nsm.setFisico(smc.isFisico());
				nsm.setLogico(smc.isLogico());
				nsm.setControlCalidad(smc.isControl());
				res.add(nsm);
				smrp.save(nsm); 
			}
		}
		return res;
	}
	@Override
	public List<ServicioManufactura> listarServicios() {
		return smrp.findAll();
	}
	@Override
	public Empresa buscarEmpresa() {
		List<Empresa> emp = erp.findAll();
		if(emp.size() == 1) {
			return emp.get(0);
		}
		return null;
	}
	@Override
	public List<Organizacion> buscarTodasOrganizaciones() {
		return orp.findAll();
	}
	@Override
	public List<ServicioManufactura> buscarServicioSinProveedor() {
		List<ProveedorServicio> listaProveedor = psrp.findAll();
		List<ServicioManufactura> listaServicioSinProveedor = new ArrayList<>();
		for(ProveedorServicio ps : listaProveedor) {
			if(ps.getUnidadFuncional() == null) {
				listaServicioSinProveedor.add(ps.getServicio());
			}
		}
		return listaServicioSinProveedor;
	}
	@Override
	public List<UnidadFuncional> buscarProveedor(ServicioManufactura sm) {
		List<UnidadFuncional> res = new ArrayList<>();
		List<ProveedorServicio> lps = psrp.findAll();
		if(lps != null) {
			for(ProveedorServicio ps : lps) {
				if(ps.getServicio()!=null) {
					if(ps.getServicio().getId() == sm.getId())
						res.add(ps.getUnidadFuncional());					
				}
			}
		}
		return res;
	}
	@Override
	public void asociarUnidadFuncionalServicios() {
		List<ProveedorServicio> lps = psrp.findAll();
		List<ServicioManufactura> lsm = smrp.findAll();
		for(ProveedorServicio ps : lps) {
			ServicioManufactura sm = ps.getServicio();
			if(sm == null) {
				for(ServicioManufactura smb : lsm) {
					if(smb.getNombreServicio().contentEquals(ps.getNombreServicio()) ){
						ps.setServicio(smb);
						psrp.save(ps);
						break;
					}	
				}
			}	
		}	
	}



}
