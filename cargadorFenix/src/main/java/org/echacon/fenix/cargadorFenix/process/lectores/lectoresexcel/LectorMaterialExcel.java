package org.echacon.fenix.cargadorFenix.process.lectores.lectoresexcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.echacon.fenix.cargadorFenix.process.contenedores.MateriaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.PropiedadCont;

public class LectorMaterialExcel {
	private List<MateriaCont> listaMateriales;
	private String error;
	public List<MateriaCont> getListaMateriales() {
		return listaMateriales;
	}
	public void setListaMateriales(List<MateriaCont> listaMateriales) {
		this.listaMateriales = listaMateriales;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public int leerArchivo(String nomArchivo) {
		int res = 0;
		listaMateriales = new ArrayList<>();
		try {
			FileInputStream archivo = new FileInputStream(new File(nomArchivo));
			XSSFWorkbook libro = new XSSFWorkbook(archivo);
			XSSFSheet hoja = libro.getSheetAt(0);
			Iterator<Row> itFila =  hoja.iterator();
			boolean fin = false;
			Row fila;
			Cell celda;
			String contenido;
			int contccr = -1;
			// eliminacion de la primera fila.
			fila = itFila.next(); 
			celda = fila.getCell(0);
			String lugar = celda.getStringCellValue();
			celda = fila.getCell(0);
			while(itFila.hasNext() && !fin) {
				fila = itFila.next();
				celda = fila.getCell(0);
				String valor = celda.getStringCellValue().trim();
				// Eliminando posibles lineas vacias en el excel
				if(valor.toLowerCase().contentEquals("fin")) {
					fin = true;
				}
				else {
					MateriaCont nm = new MateriaCont();
					nm.setListaPropiedades(new ArrayList<PropiedadCont>());
					nm.setNombre(valor);
					celda = fila.getCell(1);
					nm.setCodigo(celda.getStringCellValue());
					celda = fila.getCell(2);
					nm.setDescripcion(celda.getStringCellValue());
					celda = fila.getCell(3);
					// tipo si es granel o unidad
					valor = celda.getStringCellValue();
					valor = valor.trim().toLowerCase();
					if(valor.equals("granel")) {
						nm.setGranel(true);
					}
					if(valor.equals("unidad")) {
						nm.setUnidad(true);
					}
					// tipo de materia
					celda = fila.getCell(4);
					valor = celda.getStringCellValue();
					valor = valor.trim().toLowerCase();
					if(valor.equals("solido")) {
						nm.setSolido(true);
					}
					if(valor.equals("liquido")) {
						nm.setLiquido(true);
					}
					if(valor.equals("gas")) {
						nm.setGas(true);
					}
					celda = fila.getCell(5);
					valor = celda.getStringCellValue();
					valor = valor.trim().toLowerCase();
					if(valor.equals("si")) {
						nm.setAdquirido(true);
					}
					celda = fila.getCell(6);
					valor = celda.getStringCellValue();
					valor = valor.trim().toLowerCase();
					if(valor.equals("si")) {
						nm.setProducido(true);
					}				
					listaMateriales.add(nm);					
				}
			}
			
		} catch (IOException e) {
			error = "Error de abriendo archivo";
			e.printStackTrace();
			return -1;
		}	
		return 1;
	}
}
