package org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.echacon.fenix.cargadorFenix.process.contenedores.EmpresaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.OrganizacionCont;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorOrganizacionXML {
	private List<OrganizacionCont> organizaciones;

	public List<OrganizacionCont> getOrganizaciones() {
		return organizaciones;
	}

	public void setOrganizaciones(List<OrganizacionCont> organizaciones) {
		this.organizaciones= organizaciones;
	}
	
	public int leer(String nomArch) {
		int res = 0;
		OrganizacionCont orgc;
		File fXmlFile = new File(nomArch);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList nOrganizacion = doc.getElementsByTagName("organizacion");

			if(nOrganizacion.getLength() == 1) {
				organizaciones = new ArrayList<>();
				Node organizacion = nOrganizacion.item(0);
				NodeList orgNodeList = organizacion.getChildNodes();
				for(int i = 0; i < orgNodeList.getLength(); i++) {
					Node nodo = orgNodeList.item(i);
					if(nodo.getNodeName() == "unidad") {
						orgc = new OrganizacionCont();
						NodeList nElementos = nodo.getChildNodes();
						for(int j=0;j<nElementos.getLength();j++) {
							Node elemento = nElementos.item(j);
							if(elemento.getNodeName() == "nombre") {
								orgc.setNombre(elemento.getTextContent().trim());
							}
							if(elemento.getNodeName() == "descripcion") {
								orgc.setDescripcion(elemento.getTextContent().trim());
							}
							if(elemento.getNodeName() == "padre") {
								orgc.setOrganizacionPadre(elemento.getTextContent().trim());
							}
						}
						organizaciones.add(orgc);
					}
				}
				res =1;
			} else {
				res = -1;
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}

}
