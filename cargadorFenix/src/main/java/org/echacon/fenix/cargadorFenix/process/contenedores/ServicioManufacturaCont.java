package org.echacon.fenix.cargadorFenix.process.contenedores;

public class ServicioManufacturaCont {
	private String nombre;
	private String descripcion;
	private boolean fisico;
	private boolean control;
	private boolean logico;
	private boolean ingenieriaDesarrollo;
	private boolean controlCalidad;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isFisico() {
		return fisico;
	}
	public void setFisico(boolean fisico) {
		this.fisico = fisico;
	}
	public boolean isControl() {
		return control;
	}
	public void setControl(boolean control) {
		this.control = control;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isLogico() {
		return logico;
	}
	public void setLogico(boolean logico) {
		this.logico = logico;
	}
	public boolean isIngenieriaDesarrollo() {
		return ingenieriaDesarrollo;
	}
	public void setIngenieriaDesarrollo(boolean ingenieriaDesarrollo) {
		this.ingenieriaDesarrollo = ingenieriaDesarrollo;
	}
	public boolean isControlCalidad() {
		return controlCalidad;
	}
	public void setControlCalidad(boolean controlCalidad) {
		this.controlCalidad = controlCalidad;
	}
	
}
