package org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.echacon.fenix.cargadorFenix.process.contenedores.ServicioManufacturaCont;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorServiciosManufacturaXML {
	private List<ServicioManufacturaCont> listaServicio = new ArrayList<>();

	public List<ServicioManufacturaCont> getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List<ServicioManufacturaCont> listaServicio) {
		this.listaServicio = listaServicio;
	}
	
	public int leer(String nomArch) {
		int res = 0;
		File fXmlFile = new File(nomArch);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nServMan = doc.getElementsByTagName("serviciosManufactura");
			if(nServMan.getLength() == 1) {
				listaServicio = new ArrayList<>();
				Node servicioMan = nServMan.item(0);
				NodeList lsNodeList = servicioMan.getChildNodes();
				for(int i = 0; i < lsNodeList.getLength(); i++) {
					Node nodo = lsNodeList.item(i);
					if(nodo.getNodeName() == "servicio") {
						ServicioManufacturaCont sm = new ServicioManufacturaCont();
						//trabajar con los nodos dentro de la declaracion servicio
						NodeList elementosServicio = nodo.getChildNodes();
						for(int j =0; j<elementosServicio.getLength(); j++) {
							Node elemento = elementosServicio.item(j);
							if(elemento.getNodeName()=="nombre") {
								sm.setNombre(elemento.getTextContent().trim());
							}
							if(elemento.getNodeName()=="descripcion") {
								sm.setDescripcion(elemento.getTextContent().trim());
							}
							if(elemento.getNodeName()=="tipos") {
								NodeList nodosTipo = elemento.getChildNodes();
								for(int k=0; k<nodosTipo.getLength();k++) {
									Node tipo = nodosTipo.item(k);
									if(tipo.getNodeName()=="tipo") {
										String contenido = tipo.getTextContent().trim();
										switch (contenido) {
										case "fisico":
										case "físico":
											sm.setFisico(true);
											break;
										case "logico":
										case "lógico":
											sm.setLogico(true);
											break;
										case "control":
											sm.setControl(true);
											break;
										case "control calidad":
											sm.setControlCalidad(true);
											break;
										case "ingenieria y desarrollo":
											sm.setIngenieriaDesarrollo(true);
										}
									}
								}
							}
						}
						listaServicio.add(sm);
					}
				}
				res = 1;
			}
			else
				res= -1;
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
}
