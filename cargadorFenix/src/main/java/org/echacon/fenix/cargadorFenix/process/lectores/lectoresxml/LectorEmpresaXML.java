package org.echacon.fenix.cargadorFenix.process.lectores.lectoresxml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.echacon.fenix.cargadorFenix.process.contenedores.EmpresaCont;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorEmpresaXML {
	private EmpresaCont ec;

	public EmpresaCont getEc() {
		return ec;
	}

	public void setEc(EmpresaCont ec) {
		this.ec = ec;
	}
	
	public int leer(String nomArch) {
		int res = 0;
		
		File fXmlFile = new File(nomArch);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList nEmpresas = doc.getElementsByTagName("empresa");
			if(nEmpresas.getLength() == 1) {
				ec = new EmpresaCont();
				Node empresa = nEmpresas.item(0);
				NodeList ecNodeList = empresa.getChildNodes();
				for(int i = 0; i < ecNodeList.getLength(); i++) {
					Node nodo = ecNodeList.item(i);
					if(nodo.getNodeName() == "nombre") {
						ec.setNombre(nodo.getTextContent().trim());
					}
					if(nodo.getNodeName() == "descripcion") {
						ec.setDescripcion(nodo.getTextContent().trim());
					}
					if(nodo.getNodeName() == "descripcion") {
						ec.setContacto(nodo.getTextContent().trim());
					}
				}
				res =1;
			} else {
				res = -1;
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
}
