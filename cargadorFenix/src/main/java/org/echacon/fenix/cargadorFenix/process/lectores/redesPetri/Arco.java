package org.echacon.fenix.cargadorFenix.process.lectores.redesPetri;


public class Arco {
	private String nombre;
	private Transicion transicion;
	private Lugar lugar;
	private RedPetri red;
	private int peso;
	private boolean entrada;
	private boolean inhibidor;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Transicion getTransicion() {
		return transicion;
	}
	public void setTransicion(Transicion transicion) {
		this.transicion = transicion;
	}
	public Lugar getLugar() {
		return lugar;
	}
	public void setLugar(Lugar lugar) {
		this.lugar = lugar;
	}
	public RedPetri getRed() {
		return red;
	}
	public void setRed(RedPetri red) {
		this.red = red;
	}
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
	public boolean isEntrada() {
		return entrada;
	}
	public void setEntrada(boolean entrada) {
		this.entrada = entrada;
	}
	public boolean isInhibidor() {
		return inhibidor;
	}
	public void setInhibidor(boolean inhibidor) {
		this.inhibidor = inhibidor;
	}
	
	public void enlazar(boolean entrada, boolean inhibidor, Lugar lugar, Transicion transicion, int peso) {
		this.entrada = entrada;
		this.inhibidor = inhibidor;
		this.lugar = lugar;
		this.transicion = transicion;
		this.peso = peso;
		lugar.agregarArco(this);
		transicion.agregarArco(this);
	}
}
