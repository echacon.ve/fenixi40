package org.echacon.fenix.cargadorFenix.service;

import java.util.ArrayList;
import java.util.List;

import org.echacon.fenix.cargadorFenix.process.contenedores.InsumoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ModeloProductoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ProductoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.PropiedadCont;
import org.echacon.fenix.entity.materia.Materia;
import org.echacon.fenix.entity.materia.PropiedadMateria;
import org.echacon.fenix.entity.producto.Insumo;
import org.echacon.fenix.entity.producto.ModeloProducto;
import org.echacon.fenix.entity.producto.Producto;
import org.echacon.fenix.entity.producto.SecuenciaDeEtapas;
import org.echacon.fenix.repository.materia.MateriaRepository;
import org.echacon.fenix.repository.materia.PropiedadMateriaRepository;
import org.echacon.fenix.repository.producto.InsumoRepository;
import org.echacon.fenix.repository.producto.ModeloProductoRepository;
import org.echacon.fenix.repository.producto.ProductoRepository;
import org.echacon.fenix.repository.producto.SecuenciaDeEtapasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("productoService")
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	ProductoRepository prodrp;
	@Autowired
	InsumoRepository inrp;
	@Autowired
	MateriaRepository mrp;
	@Autowired
	PropiedadMateriaRepository pmrp;
	@Autowired
	ModeloProductoRepository mdprp;
	@Autowired
	SecuenciaDeEtapasRepository sqrp;
	
	String error;
	int codigoError;
	
	
	@Override
	public Producto crearProducto(ProductoCont prodCont) {
		error = "";
		codigoError = 0;
		List<Producto> productos = prodrp.findByNombre(prodCont.getNombre());
		if(productos.size() != 0) { 
			error = "El producto existe";
			codigoError = 1;
			return null;
		}
		Producto prod = new Producto();
		List<Materia> materias = mrp.findByNombre(prodCont.getNombre());
		Materia mat;
		if(materias.size() == 0) {
			// crear la materia
			mat = new Materia();
			mat.setNombre(prodCont.getNombre());
			mat.setPropiedades(new ArrayList<>());
			for(PropiedadCont propcont : prodCont.getPropiedades()) {
				PropiedadMateria prmat = new PropiedadMateria();
				prmat.setMateria(mat);
				prmat.setCadena(propcont.isCadena());
				prmat.setNombre(propcont.getNombre());
				prmat.setRangoNumerico(!propcont.isCadena());
				prmat.setValorCadena(propcont.getEspecificacion());
				prmat.setValMin(propcont.getValormin());
				prmat.setValMax(propcont.getValormax());
				mat.getPropiedades().add(prmat);
			}
		}
		else {
			mat = materias.get(0);
			List<PropiedadMateria> listaPropMat = pmrp.findByMateria(mat);
			mat.setPropiedades(listaPropMat);
		}
		prod.setMateria(mat);
		prod.setDescripcion(prodCont.getDescripcion());
		prod.setModelos(new ArrayList<>());
		for(ModeloProductoCont modprod : prodCont.getModelosProducto()) {
			ModeloProducto mdp = new ModeloProducto();
			mdp.setCantidadReferencia(modprod.getCantidadReferencia());
			mdp.setCodigo(modprod.getCodigo());
			mdp.setProducto(prod);
			mdp.setNombreDinamica(modprod.getNombreDinamica());
			mdp.setInsumos(new ArrayList<>());
			// buscar si el modelo de producto existe
			List<SecuenciaDeEtapas> listaSecuencias = sqrp.findByNombre(modprod.getNombreDinamica());
			SecuenciaDeEtapas sq=null;
			if(listaSecuencias.size() == 1) 
				sq=listaSecuencias.get(0);
			mdp.setDinamica(sq);
			boolean existeCodigo = true;
			for(InsumoCont ic: modprod.getInsumos()) {
				List<Materia> listaMaterias = mrp.findByCodigo(ic.getNombre());
				if(listaMaterias.size() != 1) {
					error += "El codigo del insumo" + ic.getNombre() + " no esta registrado \n";
					codigoError = 2;
				}
				else {
					Insumo ins = new Insumo();
					ins.setMaterial(listaMaterias.get(0));
					ins.setModeloProducto(mdp);
					ins.setCantidadMateria(ic.getCantidad());
					mdp.getInsumos().add(ins);
				}
				
			}
			prod.getModelos().add(mdp);
		}
		return prod;
	}

	@Override
	public void salvarProducto(Producto prod) {
		// Salvar la referencia al material. Si existia solo se persiste
		Materia mat = prod.getMateria();
		mrp.save(mat);
		for(PropiedadMateria pr: mat.getPropiedades()) {
			pmrp.save(pr);
		}
		prodrp.save(prod);
		for(ModeloProducto mdp : prod.getModelos()) {
			mdprp.save(mdp);
			for(Insumo ins : mdp.getInsumos()) {
				inrp.save(ins);
			}
		}
	}

	@Override
	public void agregarModelo(Producto prod, ModeloProducto modelo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ModeloProducto crearModeloProducto(ModeloProductoCont mpc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Producto recuperarProducto(String nombre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String obtenerError() {
	
		return error;
	}

	@Override
	public int obtenerCodigoError() {
		
		return codigoError;
	}

}
