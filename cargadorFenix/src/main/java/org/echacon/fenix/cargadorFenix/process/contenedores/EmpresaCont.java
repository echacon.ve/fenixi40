package org.echacon.fenix.cargadorFenix.process.contenedores;

public class EmpresaCont {
	private String nombre;
	private String descripcion;
	private String contacto;
	private OrganizacionCont cabeza;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public OrganizacionCont getCabeza() {
		return cabeza;
	}
	public void setCabeza(OrganizacionCont cabeza) {
		this.cabeza = cabeza;
	}
}
