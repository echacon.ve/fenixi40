package org.echacon.fenix.cargadorFenix.service;

import java.util.List;

import org.echacon.fenix.cargadorFenix.process.contenedores.MateriaCont;
import org.echacon.fenix.entity.materia.Materia;

public interface MateriaService {
	public Materia crearMateria(MateriaCont materia);
	public Materia encontrarPorNombre(String nombre);
	public Materia encontrarPorCodigo(String codigo);
	public void salvarMateria(Materia mat);
	
}
