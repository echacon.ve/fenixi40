package org.echacon.fenix.cargadorFenix.process.lectores.redesPetri;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class LectorPNML {
	private RedPetri red;
	private List<Lugar> listaLugar;
	private List<Transicion> listaTransicion;
	public RedPetri getRed() {
		return red;
	}
	public void setRed(RedPetri red) {
		this.red = red;
	}
	public List<Lugar> getListaLugar() {
		return listaLugar;
	}
	public void setListaLugar(List<Lugar> listaLugar) {
		this.listaLugar = listaLugar;
	}
	public List<Transicion> getListaTransicion() {
		return listaTransicion;
	}
	public void setListaTransicion(List<Transicion> listaTransicion) {
		this.listaTransicion = listaTransicion;
	}
	public boolean leerRed(String archivo) {
		String nombre;
		Lugar lugTrabajo = null;
		Transicion tranTrabajo = null;
		
		// Lectura de fichero_origen.xml
		try {
		File fXmlFile = new File(archivo);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		System.out.println("Leyendo archivo " + archivo);
		// Ahora documento es el XML leido en memoria.
	
		// Escritura de fichero_destino.xml
		NodeList nRedes = doc.getElementsByTagName("net");
		if(nRedes.getLength() == 1) {
			red = new RedPetri();
			red.inicializar();
			listaLugar = new ArrayList<Lugar>();
			listaTransicion = new ArrayList<Transicion>();
			Node net = nRedes.item(0);
			red.setNombre(((Element) net).getAttribute("id"));
			red.setNombre(archivo.substring(archivo.lastIndexOf("/")+1, archivo.length()));
			
			// obtener la lista de nodos en la red.
			
			NodeList netNodeList = net.getChildNodes();		
			
			for (int temp = 0; temp < netNodeList.getLength(); temp++) {
				Node nNode = netNodeList.item(temp);
				nombre = nNode.getNodeName();
				if(nombre == "place") {
					Lugar lugar = new Lugar();
					lugar.initListaArco();
					NodeList listaNodo = nNode.getChildNodes();
					String identificador = ((Element) nNode).getAttribute("id");
					lugar.setIdentificador(identificador);
					lugar.setMarcacionInicial(0);
					for(int i=0; i < listaNodo.getLength(); i++){
						Node nodo = listaNodo.item(i);
						if(nodo.getNodeName() == "name") {
							lugar.setNombre(nodo.getTextContent().trim());									
							}						
						if(nodo.getNodeName() == "initialMarking") {
							String valor = nodo.getTextContent().trim();
							lugar.setMarcacionInicial((Integer.parseInt(valor))); 
						}
					}
					listaLugar.add(lugar);
					
				}
				if(nombre == "transition") {
					Transicion transicion = new Transicion();
					transicion.initListaArco();
					NodeList listaNodo = nNode.getChildNodes();
					String identificador = ((Element) nNode).getAttribute("id");
					transicion.setIdentificador(identificador);
					transicion.setTipo(0);
					for(int i=0; i < listaNodo.getLength(); i++){
						Node nodo = listaNodo.item(i) ;
						if(nodo.getNodeName() == "name") {
							transicion.setNombre(nodo.getTextContent().trim());									
							}
						if(nodo.getNodeName() == "toolspecific") {
							NodeList listaWoPeD = nodo.getChildNodes();
							for(int iListaWoPeD = 0; iListaWoPeD < listaWoPeD.getLength(); iListaWoPeD++) {
								Node nodoListaWoPeD = listaWoPeD.item(iListaWoPeD);
								if(nodoListaWoPeD.getNodeName() == "trigger") {
									String tipoString = ((Element) nodoListaWoPeD).getAttribute("type");
									tipoString = tipoString.trim();
									transicion.setTipo(Integer.parseInt(tipoString));
								}
							}
						}
					}
					listaTransicion.add(transicion);
				}
				if(nombre == "arc") {
					Arco arco = new Arco();
					String idOrigen = ((Element) nNode).getAttribute("source");
					idOrigen = idOrigen.trim();
					String idDestino = ((Element) nNode).getAttribute("target");
					idDestino=idDestino.trim();
					String idNombre =  ((Element) nNode).getAttribute("id");
					arco.setNombre(idNombre);
					red.agregarArco(arco);
					arco.setRed(red);
	
					boolean origenLugar = false;
					boolean destinoLugar = false;
					for(int i=0; i<listaLugar.size(); i++) {
						String identificador = listaLugar.get(i).getIdentificador();
						if(idOrigen.equals(identificador.trim())){
							origenLugar = true;
							lugTrabajo = listaLugar.get(i);
							break;
						}
						if(idDestino.equals(identificador)) {
							destinoLugar = true;
							lugTrabajo = listaLugar.get(i);
							break;
						}
					}
					boolean origenTransicion = false;
					boolean destinoTransicion = false;
					for(int i = 0; i < listaTransicion.size(); i++) {
						String identificador = listaTransicion.get(i).getIdentificador();
						
						if(idOrigen.equals(identificador) ) {
							tranTrabajo = listaTransicion.get(i); 
							origenTransicion = true;
							break;
						}
						if(idDestino.equals(identificador)) {
							tranTrabajo = listaTransicion.get(i);
							destinoTransicion = true;	
						}
						
					}
					if(origenTransicion && destinoLugar) {
						arco.enlazar(false, false, lugTrabajo, tranTrabajo, 1);
					}
					if(origenLugar && destinoTransicion) {
						arco.enlazar(true,false, lugTrabajo, tranTrabajo, 1);
					}
				}
			}
		}
		
		for(int i=0; i<listaLugar.size(); i++) {
			listaLugar.get(i).setRed(red);
			red.agregarLugar(listaLugar.get(i));
		}
		for(int i=0; i<listaTransicion.size(); i++) {
			listaTransicion.get(i).setRed(red);
			red.agregarTransicion(listaTransicion.get(i));
	}

}
catch (Exception e) {
	e.printStackTrace();
	return false;
	}
	
	return true;
}
}
