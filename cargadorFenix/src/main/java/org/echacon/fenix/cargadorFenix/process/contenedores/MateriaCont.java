package org.echacon.fenix.cargadorFenix.process.contenedores;

import java.util.ArrayList;
import java.util.List;

public class MateriaCont {
	private String nombre;
	private String codigo;
	private String descripcion;
	private boolean granel;
	private boolean unidad;
	private boolean gas;
	private boolean liquido;
	private boolean solido;
	private boolean adquirido;
	private boolean producido;
	private List<PropiedadCont> listaPropiedades;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isGranel() {
		return granel;
	}
	public void setGranel(boolean granel) {
		this.granel = granel;
	}
	public boolean isUnidad() {
		return unidad;
	}
	public void setUnidad(boolean unidad) {
		this.unidad = unidad;
	}
	public boolean isGas() {
		return gas;
	}
	public void setGas(boolean gas) {
		this.gas = gas;
	}
	public boolean isLiquido() {
		return liquido;
	}
	public void setLiquido(boolean liquido) {
		this.liquido = liquido;
	}
	public boolean isSolido() {
		return solido;
	}
	public void setSolido(boolean solido) {
		this.solido = solido;
	}
	public boolean isAdquirido() {
		return adquirido;
	}
	public void setAdquirido(boolean adquirido) {
		this.adquirido = adquirido;
	}
	public boolean isProducido() {
		return producido;
	}
	public void setProducido(boolean producido) {
		this.producido = producido;
	}
	public List<PropiedadCont> getListaPropiedades() {
		return listaPropiedades;
	}
	public void setListaPropiedades(List<PropiedadCont> listaPropiedades) {
		this.listaPropiedades = listaPropiedades;
	}
	
}
