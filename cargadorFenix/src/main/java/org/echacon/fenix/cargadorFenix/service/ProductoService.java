package org.echacon.fenix.cargadorFenix.service;

import org.echacon.fenix.cargadorFenix.process.contenedores.ModeloProductoCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ProductoCont;
import org.echacon.fenix.entity.producto.ModeloProducto;
import org.echacon.fenix.entity.producto.Producto;

public interface ProductoService {
	public Producto crearProducto(ProductoCont prodCont);
	public void salvarProducto(Producto prod);
	public void agregarModelo(Producto prod, ModeloProducto modelo);
	public ModeloProducto crearModeloProducto(ModeloProductoCont mpc);
	public Producto recuperarProducto(String nombre);
	public String obtenerError();
	public int obtenerCodigoError();
	
}
