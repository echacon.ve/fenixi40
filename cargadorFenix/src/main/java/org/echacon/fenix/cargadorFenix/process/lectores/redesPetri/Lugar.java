package org.echacon.fenix.cargadorFenix.process.lectores.redesPetri;

import java.util.ArrayList;
import java.util.List;



public class Lugar {
	private String identificador;
	private String nombre;
	private int marcacionInicial;
	private RedPetri red;
	private List<Arco> listaArco;
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getMarcacionInicial() {
		return marcacionInicial;
	}
	public void setMarcacionInicial(int marcacionInicial) {
		this.marcacionInicial = marcacionInicial;
	}
	public RedPetri getRed() {
		return red;
	}
	public void setRed(RedPetri red) {
		this.red = red;
	}
	public List<Arco> getListaArco() {
		return listaArco;
	}
	public void setListaArco(List<Arco> listaArco) {
		this.listaArco = listaArco;
	}
	
	public void agregarArco(Arco arco){
		listaArco.add(arco);
	}
	public void initListaArco(){
		listaArco = new ArrayList<Arco>();
	}
}
