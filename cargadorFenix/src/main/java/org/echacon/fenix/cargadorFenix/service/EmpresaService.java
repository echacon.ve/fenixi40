package org.echacon.fenix.cargadorFenix.service;

import java.util.List;

import org.echacon.fenix.cargadorFenix.process.contenedores.EmpresaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.OrganizacionCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.ServicioManufacturaCont;
import org.echacon.fenix.cargadorFenix.process.contenedores.UnidadFuncionalCont;
import org.echacon.fenix.entity.base.Empresa;
import org.echacon.fenix.entity.base.Organizacion;
import org.echacon.fenix.entity.recurso.UnidadFuncional;
import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;

public interface EmpresaService {
	public Empresa crearEmpresa(EmpresaCont ec);
	public Empresa buscarEmpresa();
	public void salvar(Empresa emp);
	public Organizacion crearOrganizacion(OrganizacionCont oc);
	public Organizacion buscarOrganizacion(String nombre);
	public List<Organizacion> crearVariasOrg(List<OrganizacionCont> organizacion);
	public List<UnidadFuncional> crearListaUnidadesFuncionales(List<UnidadFuncionalCont> lista);
	public void salvarOrganizacion(Organizacion org);
	public boolean asociarOrgAEmp(Empresa emp, Organizacion org);
	public boolean asociarOrgAOrg(Organizacion org1, Organizacion org2);
	public ServicioManufactura crearServ(ServicioManufacturaCont smc);
	public List<ServicioManufactura> crearListaServicios(List<ServicioManufacturaCont> listasmc);
	public List<ServicioManufactura> listarServicios();
	public List<Organizacion> buscarTodasOrganizaciones();
	public List<ServicioManufactura> buscarServicioSinProveedor();
	public List<UnidadFuncional> buscarProveedor(ServicioManufactura sm);
	public void asociarUnidadFuncionalServicios();
	
	
	
}
