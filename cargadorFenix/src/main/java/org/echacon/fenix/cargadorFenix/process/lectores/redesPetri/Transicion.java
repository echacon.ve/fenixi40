package org.echacon.fenix.cargadorFenix.process.lectores.redesPetri;

import java.util.ArrayList;
import java.util.List;


public class Transicion {
	private RedPetri red;
	private String identificador;
	private String nombre;
	private int tipo;
	private List<Arco> listaArco;
	public RedPetri getRed() {
		return red;
	}
	public void setRed(RedPetri red) {
		this.red = red;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public List<Arco> getListaArco() {
		return listaArco;
	}
	public void setListaArco(List<Arco> listaArco) {
		this.listaArco = listaArco;
	}
	
	public void agregarArco(Arco arco){
		listaArco.add(arco);
	}
	public void initListaArco(){
		listaArco = new ArrayList<Arco>();
	}
}
