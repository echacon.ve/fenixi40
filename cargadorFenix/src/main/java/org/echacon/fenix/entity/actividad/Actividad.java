package org.echacon.fenix.entity.actividad;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Entity
@Table(name="act_actividad")
public class Actividad implements Serializable{
	@Id @GeneratedValue
	private long id;
	private LocalDateTime fechaInicioEsperada;
	private LocalDateTime fechaInicio;
	private LocalDateTime fechaTerminacionEsperada;
	private LocalDateTime fechaTerminacion;
	private long duracion;
	private boolean programada;
	private boolean enEjecucion;
	private boolean culminadaConExito;
	private boolean suspendida;
	@OneToOne
	private EventoActividad eventoInicio;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public LocalDateTime getFechaInicioEsperada() {
		return fechaInicioEsperada;
	}
	public void setFechaInicioEsperada(LocalDateTime fechaInicioEsperada) {
		this.fechaInicioEsperada = fechaInicioEsperada;
	}
	public LocalDateTime getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(LocalDateTime fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public LocalDateTime getFechaTerminacionEsperada() {
		return fechaTerminacionEsperada;
	}
	public void setFechaTerminacionEsperada(LocalDateTime fechaTerminacionEsperada) {
		this.fechaTerminacionEsperada = fechaTerminacionEsperada;
	}
	public LocalDateTime getFechaTerminacion() {
		return fechaTerminacion;
	}
	public void setFechaTerminacion(LocalDateTime fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}
	public long getDuracion() {
		return duracion;
	}
	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}
	

}
