package org.echacon.fenix.entity.producto;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.echacon.fenix.entity.materia.Materia;
@Entity
@Table(name="prod_insumo")
public class Insumo implements Serializable{
	@Id @GeneratedValue
	private long id;
	@ManyToOne
	private Materia material;
	@ManyToOne
	private ModeloProducto modeloProducto;
	private float cantidadMateria;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Materia getMaterial() {
		return material;
	}
	public void setMaterial(Materia material) {
		this.material = material;
	}
	public ModeloProducto getModeloProducto() {
		return modeloProducto;
	}
	public void setModeloProducto(ModeloProducto modeloProducto) {
		this.modeloProducto = modeloProducto;
	}
	public float getCantidadMateria() {
		return cantidadMateria;
	}
	public void setCantidadMateria(float cantidadMateria) {
		this.cantidadMateria = cantidadMateria;
	}
	
}
