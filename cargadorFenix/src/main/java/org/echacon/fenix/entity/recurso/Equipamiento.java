package org.echacon.fenix.entity.recurso;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="rec_equipamiento")
public class Equipamiento implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=64)
	private String descripcion;
	private LocalDate fechaAsignacion;
	@ManyToOne
	private UnidadFuncional unidadFuncional;
	@ManyToOne
	private Equipo equipo;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public LocalDate getFechaAsignacion() {
		return fechaAsignacion;
	}
	public void setFechaAsignacion(LocalDate fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}
	public UnidadFuncional getUnidadFuncional() {
		return unidadFuncional;
	}
	public void setUnidadFuncional(UnidadFuncional unidadFuncional) {
		this.unidadFuncional = unidadFuncional;
	}
	public Equipo getEquipo() {
		return equipo;
	}
	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

}
