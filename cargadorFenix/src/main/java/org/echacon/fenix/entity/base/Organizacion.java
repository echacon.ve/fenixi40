package org.echacon.fenix.entity.base;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.echacon.fenix.entity.recurso.UnidadFuncional;

@Entity
@Table(name="bases_organizacion")
public class Organizacion implements Serializable{
	@Id @GeneratedValue
	private long Id;
	@Column(length = 32)
	private String nombre;
	@Column(length = 128)
	private String descripcion;
	@OneToMany(mappedBy="dependeDe")
	private List<Organizacion> dependientes;
	@ManyToOne
	private Organizacion dependeDe;
	@OneToMany(mappedBy="organizacion")
	private List<UnidadFuncional> unidadFuncional;
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<Organizacion> getDependientes() {
		return dependientes;
	}
	public void setDependientes(List<Organizacion> dependientes) {
		this.dependientes = dependientes;
	}
	public Organizacion getDependeDe() {
		return dependeDe;
	}
	public void setDependeDe(Organizacion dependeDe) {
		this.dependeDe = dependeDe;
	}
	public List<UnidadFuncional> getUnidadFuncional() {
		return unidadFuncional;
	}
	public void setUnidadFuncional(List<UnidadFuncional> unidadFuncional) {
		this.unidadFuncional = unidadFuncional;
	}
	
	
}
