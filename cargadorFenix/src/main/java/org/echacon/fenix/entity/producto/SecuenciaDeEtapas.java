package org.echacon.fenix.entity.producto;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="prod_secuencia_etapas")
public class SecuenciaDeEtapas implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String codigo;
	@Column(length=64)
	private String nombre;
	@Column(length=256)
	private String descripcion;
	@OneToMany(mappedBy="secuencia")
	List<EtapaProducto> etapas;
	@OneToMany(mappedBy="secuencia")
	List<TransicionEtapa> transiciones;
	@OneToMany(mappedBy="secuencia")
	List<ArcoSecProd> arcos;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<EtapaProducto> getEtapas() {
		return etapas;
	}
	public void setEtapas(List<EtapaProducto> etapas) {
		this.etapas = etapas;
	}
	public List<TransicionEtapa> getTransiciones() {
		return transiciones;
	}
	public void setTransiciones(List<TransicionEtapa> transiciones) {
		this.transiciones = transiciones;
	}
	public List<ArcoSecProd> getArcos() {
		return arcos;
	}
	public void setArcos(List<ArcoSecProd> arcos) {
		this.arcos = arcos;
	}
	

	
}
