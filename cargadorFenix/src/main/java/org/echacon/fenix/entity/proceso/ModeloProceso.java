package org.echacon.fenix.entity.proceso;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="proc_modelo_proceso")
public class ModeloProceso implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=128)
	private String descripcion;
	@OneToMany(mappedBy = "modeloProceso")
	private List<Paso> listaPasos;
	@OneToMany(mappedBy = "modeloProceso")
	private List<TransicionPaso> listaTransiciones;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<Paso> getListaPasos() {
		return listaPasos;
	}
	public void setListaPasos(List<Paso> listaPasos) {
		this.listaPasos = listaPasos;
	}
	

}
