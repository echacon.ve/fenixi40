package org.echacon.fenix.entity.producto;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="prod_arc_sec_prod")
public class ArcoSecProd implements Serializable{
	@Id @GeneratedValue
	private long id;
	@ManyToOne
	private SecuenciaDeEtapas secuencia;
	private boolean transEtapa;
	@ManyToOne
	private EtapaProducto etapa;
	@ManyToOne
	private TransicionEtapa transicion;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public SecuenciaDeEtapas getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(SecuenciaDeEtapas secuencia) {
		this.secuencia = secuencia;
	}
	public boolean isTransEtapa() {
		return transEtapa;
	}
	public void setTransEtapa(boolean transEtapa) {
		this.transEtapa = transEtapa;
	}
	public EtapaProducto getEtapa() {
		return etapa;
	}
	public void setEtapa(EtapaProducto etapa) {
		this.etapa = etapa;
	}
	public TransicionEtapa getTransicion() {
		return transicion;
	}
	public void setTransicion(TransicionEtapa transicion) {
		this.transicion = transicion;
	}
	
	
	


}
