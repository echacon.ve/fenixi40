package org.echacon.fenix.entity.recurso;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rec_modelo_equipo")
public class ModeloEquipo implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length = 64)
	private String nombre;
	@Column(length = 64)
	private String serialModelo;
	@Column(length = 64)
	private String nombreFabricante;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSerialModelo() {
		return serialModelo;
	}
	public void setSerialModelo(String serialModelo) {
		this.serialModelo = serialModelo;
	}
	public String getNombreFabricante() {
		return nombreFabricante;
	}
	public void setNombreFabricante(String nombreFabricante) {
		this.nombreFabricante = nombreFabricante;
	}
	
	

}
