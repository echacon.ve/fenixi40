package org.echacon.fenix.entity.materia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="materia_tipo_material_atributo")
public class TipoPropiedadMateria implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=256)
	private String descripcion;
	@Column(length=64)
	private boolean literal;
	private boolean rango;
	private boolean valorUnico;
	private String valorLiteral;
	private float valMin;
	private float valMax;
	@ManyToOne
	private TipoMaterial tipoMaterial;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isLiteral() {
		return literal;
	}
	public void setLiteral(boolean literal) {
		this.literal = literal;
	}
	public boolean isRango() {
		return rango;
	}
	public void setRango(boolean rango) {
		this.rango = rango;
	}
	public boolean isValorUnico() {
		return valorUnico;
	}
	public void setValorUnico(boolean valorUnico) {
		this.valorUnico = valorUnico;
	}
	public String getValorLiteral() {
		return valorLiteral;
	}
	public void setValorLiteral(String valorLiteral) {
		this.valorLiteral = valorLiteral;
	}
	public float getValMin() {
		return valMin;
	}
	public void setValMin(float valMin) {
		this.valMin = valMin;
	}
	public float getValMax() {
		return valMax;
	}
	public void setValMax(float valMax) {
		this.valMax = valMax;
	}
	public TipoMaterial getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(TipoMaterial tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}
	

}
