package org.echacon.fenix.entity.base;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author edgar
 *  Unidades de Medida usadas en la organizacion
 *  
 */
@Entity
@Table(name="bases_unidad_medida")
public class UnidadMedida implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=32)
	private String nombre;
	@Column(length=256)
	private String descripcion;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}
