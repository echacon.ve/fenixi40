package org.echacon.fenix.entity.recurso;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="rec_equipo")
public class Equipo implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length = 64)
	private String identificadorEquipo;
	@ManyToOne
	private ModeloEquipo modeloEquipo;
	@ManyToOne
	private Equipamiento ubicacion;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdentificadorEquipo() {
		return identificadorEquipo;
	}
	public void setIdentificadorEquipo(String identificadorEquipo) {
		this.identificadorEquipo = identificadorEquipo;
	}
	public ModeloEquipo getModeloEquipo() {
		return modeloEquipo;
	}
	public void setModeloEquipo(ModeloEquipo modeloEquipo) {
		this.modeloEquipo = modeloEquipo;
	}
	public Equipamiento getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(Equipamiento ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	

}
