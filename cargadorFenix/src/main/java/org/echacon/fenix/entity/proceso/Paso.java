package org.echacon.fenix.entity.proceso;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="proc_paso")
public class Paso implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=128)
	private String descripcion;
	@ManyToOne
	private ModeloProceso modeloProceso;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public ModeloProceso getModeloProceso() {
		return modeloProceso;
	}
	public void setModeloProceso(ModeloProceso modeloProceso) {
		this.modeloProceso = modeloProceso;
	}
	

}
