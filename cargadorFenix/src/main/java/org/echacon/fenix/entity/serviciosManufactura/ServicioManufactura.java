package org.echacon.fenix.entity.serviciosManufactura;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="serv_servicio_manufactura")
public class ServicioManufactura implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=128)
	private String nombreServicio;
	@Column(length=128)
	private String descripcion;
	private boolean fisico;
	private boolean logico;
	private boolean controlCalidad;
	private boolean ingenieriaDesarrollo;
	@OneToMany(mappedBy="servicio")
	private List<ProveedorServicio> proveedores;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombreServicio() {
		return nombreServicio;
	}
	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isFisico() {
		return fisico;
	}
	public void setFisico(boolean fisico) {
		this.fisico = fisico;
	}
	public boolean isLogico() {
		return logico;
	}
	public void setLogico(boolean logico) {
		this.logico = logico;
	}
	
	public boolean isControlCalidad() {
		return controlCalidad;
	}
	public void setControlCalidad(boolean controlCalidad) {
		this.controlCalidad = controlCalidad;
	}
	public boolean isIngenieriaDesarrollo() {
		return ingenieriaDesarrollo;
	}
	public void setIngenieriaDesarrollo(boolean ingenieriaDesarrollo) {
		this.ingenieriaDesarrollo = ingenieriaDesarrollo;
	}
	public List<ProveedorServicio> getProveedores() {
		return proveedores;
	}
	public void setProveedores(List<ProveedorServicio> proveedores) {
		this.proveedores = proveedores;
	}
	
	

}
