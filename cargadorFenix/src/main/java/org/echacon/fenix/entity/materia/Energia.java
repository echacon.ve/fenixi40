package org.echacon.fenix.entity.materia;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.echacon.fenix.entity.base.UnidadMedida;

@Entity
@Table(name="materia_energia")
public class Energia implements Serializable{
	@Id
	@GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=128)
	private String descripcion;
	@ManyToOne
	private UnidadMedida unidad;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public UnidadMedida getUnidad() {
		return unidad;
	}
	public void setUnidad(UnidadMedida unidad) {
		this.unidad = unidad;
	}

}
