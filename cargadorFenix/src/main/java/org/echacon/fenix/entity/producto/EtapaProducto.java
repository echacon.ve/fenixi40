package org.echacon.fenix.entity.producto;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;

@Entity
@Table(name="prod_etapa_producto")
public class EtapaProducto implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length = 64)
	private String nombre;
	@Column(length = 32)
	private String idpnml;
	@ManyToOne
	private SecuenciaDeEtapas secuencia;
	@ManyToOne
	private ServicioManufactura servicio;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public SecuenciaDeEtapas getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(SecuenciaDeEtapas secuencia) {
		this.secuencia = secuencia;
	}
	public ServicioManufactura getServicio() {
		return servicio;
	}
	public void setServicio(ServicioManufactura servicio) {
		this.servicio = servicio;
	}
	
	
	
}
