package org.echacon.fenix.entity.materia;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="materia_tipo_material")
public class TipoMaterial implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=256)
	private String descripcion;
	@OneToMany(mappedBy="tipoMaterial")
	private List<TipoPropiedadMateria> listaAtributos; 
	

}
