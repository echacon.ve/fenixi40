package org.echacon.fenix.entity.materia;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="materia_material")
public class Materia implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=64)
	private String codigo;
	@Column(length=256)
	private String descripcion;
	private boolean granel;
	private boolean solido;
	private boolean liquido;
	private boolean gas;
	private boolean unidad;
	private boolean producido;
	private boolean adquirido;
	@OneToMany(mappedBy="materia")
	private List<PropiedadMateria> propiedades;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isGranel() {
		return granel;
	}
	public void setGranel(boolean granel) {
		this.granel = granel;
	}
	public boolean isSolido() {
		return solido;
	}
	public void setSolido(boolean solido) {
		this.solido = solido;
	}
	public boolean isLiquido() {
		return liquido;
	}
	public void setLiquido(boolean liquido) {
		this.liquido = liquido;
	}
	public boolean isGas() {
		return gas;
	}
	public void setGas(boolean gas) {
		this.gas = gas;
	}
	public boolean isUnidad() {
		return unidad;
	}
	public void setUnidad(boolean unidad) {
		this.unidad = unidad;
	}
	public boolean isProducido() {
		return producido;
	}
	public void setProducido(boolean producido) {
		this.producido = producido;
	}
	public boolean isAdquirido() {
		return adquirido;
	}
	public void setAdquirido(boolean adquirido) {
		this.adquirido = adquirido;
	}
	public List<PropiedadMateria> getPropiedades() {
		return propiedades;
	}
	public void setPropiedades(List<PropiedadMateria> propiedades) {
		this.propiedades = propiedades;
	}
	
}
