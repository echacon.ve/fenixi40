package org.echacon.fenix.entity.proceso;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.echacon.fenix.entity.recurso.UnidadFuncional;
import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;

@Entity
@Table(name="proc_proceso")
public class Proceso implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=128)
	private String descripcion;
	@ManyToOne
	private ServicioManufactura implementa;
	@ManyToOne
	private UnidadFuncional unidadFuncional;
	@ManyToOne
	private ModeloProceso comportamiento;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public ServicioManufactura getImplementa() {
		return implementa;
	}
	public void setImplementa(ServicioManufactura implementa) {
		this.implementa = implementa;
	}
	public UnidadFuncional getUnidadFuncional() {
		return unidadFuncional;
	}
	public void setUnidadFuncional(UnidadFuncional unidadFuncional) {
		this.unidadFuncional = unidadFuncional;
	}
	public ModeloProceso getComportamiento() {
		return comportamiento;
	}
	public void setComportamiento(ModeloProceso comportamiento) {
		this.comportamiento = comportamiento;
	}
	
}
