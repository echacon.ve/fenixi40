package org.echacon.fenix.entity.recurso;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.echacon.fenix.entity.base.Organizacion;
import org.echacon.fenix.entity.serviciosManufactura.ProveedorServicio;

@Entity
@Table(name="rec_unidad_funcional")
public class UnidadFuncional implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=128)
	private String descripcion;
	@OneToMany(mappedBy="unidadFuncional")
	private List<ProveedorServicio> listaServicios;
	@OneToMany(mappedBy="unidadFuncional")
	private List<Equipamiento> equipamiento;
	@ManyToOne
	private Organizacion organizacion;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<ProveedorServicio> getListaServicios() {
		return listaServicios;
	}
	public void setListaServicios(List<ProveedorServicio> listaServicios) {
		this.listaServicios = listaServicios;
	}

	public List<Equipamiento> getEquipamiento() {
		return equipamiento;
	}
	public void setEquipamiento(List<Equipamiento> equipamiento) {
		this.equipamiento = equipamiento;
	}
	public Organizacion getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}
}
