package org.echacon.fenix.entity.producto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.echacon.fenix.entity.materia.Materia;

@Entity
@Table(name="prod_producto")
public class Producto implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	@Column(length=128)
	private String descripcion;
	@ManyToOne
	private Materia materia;
	@OneToMany(mappedBy="producto")
	private List<ModeloProducto> modelos;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Materia getMateria() {
		return materia;
	}
	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	public List<ModeloProducto> getModelos() {
		return modelos;
	}
	public void setModelos(List<ModeloProducto> modelos) {
		this.modelos = modelos;
	}
}
