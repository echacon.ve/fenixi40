package org.echacon.fenix.entity.materia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="materia_propiedad_materia")
public class PropiedadMateria implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String nombre;
	private boolean cadena;
	@Column(length=64)
	private String valorCadena;
	private boolean rangoNumerico;
	private double valMin;
	private double valMax;
	@ManyToOne
	private Materia materia;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isCadena() {
		return cadena;
	}
	public void setCadena(boolean cadena) {
		this.cadena = cadena;
	}
	public String getValorCadena() {
		return valorCadena;
	}
	public void setValorCadena(String valorCadena) {
		this.valorCadena = valorCadena;
	}
	public boolean isRangoNumerico() {
		return rangoNumerico;
	}
	public void setRangoNumerico(boolean rangoNumerico) {
		this.rangoNumerico = rangoNumerico;
	}
	public double getValMin() {
		return valMin;
	}
	public void setValMin(double valMin) {
		this.valMin = valMin;
	}
	public double getValMax() {
		return valMax;
	}
	public void setValMax(double valMax) {
		this.valMax = valMax;
	}
	public Materia getMateria() {
		return materia;
	}
	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	

}
