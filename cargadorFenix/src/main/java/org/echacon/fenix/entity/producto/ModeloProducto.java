package org.echacon.fenix.entity.producto;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="prod_modelo_producto")
public class ModeloProducto implements Serializable {
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String codigo;
	@Column(length=64)
	private String nombre;
	@ManyToOne
	private Producto producto;
	private float cantidadReferencia; // especifica la cantida utilizada como patron en la formula
	@OneToMany(mappedBy="modeloProducto")
	List<Insumo> insumos;
	@ManyToOne
	private SecuenciaDeEtapas dinamica;
	private String nombreDinamica;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public float getCantidadReferencia() {
		return cantidadReferencia;
	}
	public void setCantidadReferencia(float cantidadReferencia) {
		this.cantidadReferencia = cantidadReferencia;
	}
	public List<Insumo> getInsumos() {
		return insumos;
	}
	public void setInsumos(List<Insumo> insumos) {
		this.insumos = insumos;
	}
	public SecuenciaDeEtapas getDinamica() {
		return dinamica;
	}
	public void setDinamica(SecuenciaDeEtapas dinamica) {
		this.dinamica = dinamica;
	}
	public String getNombreDinamica() {
		return nombreDinamica;
	}
	public void setNombreDinamica(String nombreDinamica) {
		this.nombreDinamica = nombreDinamica;
	}
	
	
	
	
	

}
