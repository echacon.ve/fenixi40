package org.echacon.fenix.entity.base;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author edgarchacon
 * Datos basicos de la empresa
 */
@Entity
@Table(name="bases_empresa")
public class Empresa implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length = 64)
	private String nombre;
	private LocalDate fechaSistema;
	@Column(length = 256)
	private String descripcion;
	@Column(length = 256)
	private String contacto;
	@ManyToOne
	private Organizacion cabeza;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public LocalDate getFechaSistema() {
		return fechaSistema;
	}
	public void setFechaSistema(LocalDate fechaSistema) {
		this.fechaSistema = fechaSistema;
	}
	public Organizacion getCabeza() {
		return cabeza;
	}
	public void setCabeza(Organizacion cabeza) {
		this.cabeza = cabeza;
	}
}
