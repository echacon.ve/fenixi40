package org.echacon.fenix.entity.producto;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="prod_transicion_etapa")
public class TransicionEtapa implements Serializable{
	@Id @GeneratedValue
	private long id;
	@ManyToOne
	private SecuenciaDeEtapas secuencia;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public SecuenciaDeEtapas getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(SecuenciaDeEtapas secuencia) {
		this.secuencia = secuencia;
	}
	
	
}
