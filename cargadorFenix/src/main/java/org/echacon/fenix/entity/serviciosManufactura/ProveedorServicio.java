package org.echacon.fenix.entity.serviciosManufactura;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.echacon.fenix.entity.recurso.UnidadFuncional;

@Entity
@Table(name="serv_proveedor_servicio")
public class ProveedorServicio implements Serializable{
	@Id @GeneratedValue
	private long id;
	@Column(length=64)
	private String proveedorServicio;
	@Column(length=128)
	private String nombreServicio;
	@ManyToOne
	private ServicioManufactura servicio;
	@ManyToOne
	private UnidadFuncional unidadFuncional;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProveedorServicio() {
		return proveedorServicio;
	}
	public void setProveedorServicio(String proveedorServicio) {
		this.proveedorServicio = proveedorServicio;
	}
	public String getNombreServicio() {
		return nombreServicio;
	}
	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}
	public ServicioManufactura getServicio() {
		return servicio;
	}
	public void setServicio(ServicioManufactura servicio) {
		this.servicio = servicio;
	}
	public UnidadFuncional getUnidadFuncional() {
		return unidadFuncional;
	}
	public void setUnidadFuncional(UnidadFuncional unidadFuncional) {
		this.unidadFuncional = unidadFuncional;
	}
	
	
}
