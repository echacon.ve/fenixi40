package org.echacon.fenix.repository.actividad;

import org.echacon.fenix.entity.actividad.Actividad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ActividadRepository extends JpaRepository<Actividad, Long>{

}
