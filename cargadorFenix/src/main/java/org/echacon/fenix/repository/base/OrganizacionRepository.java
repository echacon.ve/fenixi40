package org.echacon.fenix.repository.base;

import java.util.List;

import org.echacon.fenix.entity.base.Organizacion;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizacionRepository extends JpaRepository<Organizacion, Long>{
	@Query("From Organizacion as o WHERE o.nombre LIKE :nombre")
	List<Organizacion> findByNombre(@Param("nombre") String on);
}
