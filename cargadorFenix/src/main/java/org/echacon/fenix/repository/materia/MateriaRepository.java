package org.echacon.fenix.repository.materia;

import java.util.List;

import org.echacon.fenix.entity.materia.Materia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MateriaRepository extends JpaRepository<Materia, Long>{
	@Query("From Materia as mat where mat.nombre = :nombre")
	List<Materia> findByNombre(@Param("nombre") String nombre);
	@Query("From Materia as mat where mat.codigo = :codigo")
	List<Materia> findByCodigo(@Param("codigo")String codigo);
}
