package org.echacon.fenix.repository.materia;

import java.util.List;

import org.echacon.fenix.entity.materia.Materia;
import org.echacon.fenix.entity.materia.PropiedadMateria;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PropiedadMateriaRepository extends JpaRepository<PropiedadMateria,Long>{
	@Query("From PropiedadMateria as pm where pm.materia = :materia")
	List<PropiedadMateria> findByMateria(@Param("materia") Materia mat);
	
}
