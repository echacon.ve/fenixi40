package org.echacon.fenix.repository.proceso;

import org.echacon.fenix.entity.proceso.Proceso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcesoRepository extends JpaRepository<Proceso, Long>{

}
