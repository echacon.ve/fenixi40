package org.echacon.fenix.repository.producto;

import java.util.List;

import org.echacon.fenix.entity.producto.ModeloProducto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ModeloProductoRepository extends JpaRepository<ModeloProducto, Long>{
	@Query("From ModeloProducto as mp WHERE mp.nombre LIKE :nombre")
	List<ModeloProducto> findByNombre(@Param("nombre") String nombre);

}
