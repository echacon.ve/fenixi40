package org.echacon.fenix.repository.producto;

import java.util.List;

import org.echacon.fenix.entity.producto.SecuenciaDeEtapas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SecuenciaDeEtapasRepository extends JpaRepository<SecuenciaDeEtapas,Long>{
	@Query("From SecuenciaDeEtapas as sq WHERE sq.nombre LIKE :nombre")
	List<SecuenciaDeEtapas> findByNombre(@Param("nombre") String nombre);
}