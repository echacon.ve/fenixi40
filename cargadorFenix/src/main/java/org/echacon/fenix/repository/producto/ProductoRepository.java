package org.echacon.fenix.repository.producto;

import java.util.List;

import org.echacon.fenix.entity.producto.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long>{
	@Query("FROM Producto as pr WHERE pr.nombre LIKE :nombre")
	List<Producto> findByNombre(@Param("nombre") String nombre);

}
