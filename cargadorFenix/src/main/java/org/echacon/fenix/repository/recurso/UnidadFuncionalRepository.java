package org.echacon.fenix.repository.recurso;

import java.util.List;

import org.echacon.fenix.entity.recurso.UnidadFuncional;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UnidadFuncionalRepository extends JpaRepository<UnidadFuncional, Long>{
	@Query("From UnidadFuncional as uf where uf.nombre = :nombre")
	List<UnidadFuncional> findByNombre(@Param("nombre") String nombre);
}
