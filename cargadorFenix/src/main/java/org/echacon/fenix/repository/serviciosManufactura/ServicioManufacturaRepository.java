package org.echacon.fenix.repository.serviciosManufactura;

import java.util.List;

import org.echacon.fenix.entity.serviciosManufactura.ServicioManufactura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicioManufacturaRepository extends JpaRepository<ServicioManufactura, Long>{
	List<ServicioManufactura> findByNombreServicio(String nsm);
}
