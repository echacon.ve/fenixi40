package org.echacon.fenix.repository.recurso;

import org.echacon.fenix.entity.recurso.Equipamiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipamientoRepository extends JpaRepository<Equipamiento, Long>{

}
