package org.echacon.fenix.repository.base;

import java.util.List;

import org.echacon.fenix.entity.base.Empresa;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long>{
	List<Empresa> findByNombre(String nombre);
}
