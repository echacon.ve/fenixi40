package org.echacon.fenix.repository.serviciosManufactura;

import org.echacon.fenix.entity.serviciosManufactura.ProveedorServicio;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProveedorServicioRepository extends JpaRepository<ProveedorServicio, Long>{
	ProveedorServicio findByUnidadFuncionalId(Long id);
	ProveedorServicio findByServicioId(Long id);
}
